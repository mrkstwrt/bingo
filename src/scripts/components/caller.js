class Caller {

  /**
   * constructor - Creates a bingo Caller with an initial pool size
   *
   * @param {type} poolSize Initial poolsize for the Caller
   *
   */
  constructor(poolSize = 90) {
    this.poolSize = poolSize;
    this.init();
  }

  init() {
    this.reset();
  }

  /**
   * reset - Resets the pool of balls available to the caller.
   *
   * @param {int} poolSize (optional) Sets the new pool size, if not provided
   * will use the existing poolSize set by the constructor
   *
   */
  reset(poolSize) {
    this.poolSize = Number.isInteger(poolSize) ? poolSize : this.poolSize;
    this.balls = [...Array(this.poolSize + 1).keys()].slice(1);
  }

  /**
   * pick - Picks and removes a single ball from the pool
   *
   * @return {int} The ball number picked from the pool
   */
  pick() {
    const index = Math.floor(Math.random() * this.balls.length);
    return this.balls.length ? this.balls.splice(index, 1)[0] : 0;
  }
}

export default Caller;

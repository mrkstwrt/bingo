class TicketStrip {

  /**
   * constructor - Creates a TicketStrip
   *
   * @param {string}   ticketString       String representing the new ticket strip
   * @param {number} [numbersPerLine=5] Numbers per line, defaults to 5
   * @param {number} [linesPerTicket=3] Lines per ticket on the strip, defaults to 3
   *
   * @return {type} Description
   */
  constructor(ticketString, numbersPerLine = 5, linesPerTicket = 3) {
    this.ticketString = ticketString;
    this.numbersPerLine = numbersPerLine;
    this.linesPerTicket = linesPerTicket;
    this.init();
  }

  init() {
    this.numbersPerTicket = this.numbersPerLine * this.linesPerTicket;
    this.numberOfTickets = (this.ticketString.length / 2) / this.numbersPerTicket;
    this.numberOfLines = this.numberOfTickets * this.linesPerTicket;
    this.ticketArray = this.parseTicket(this.ticketString);


    if (!this.ticketArray.length) throw new Error('Invalid Ticket');
  }

  /**
   * parseTicket - Turns a ticket string into an array of number objects. Each number
   * has a line and column position, a called boolean and it's number.
   *
   * @param {string} ticketString String representing the numbers on the bingo ticket
   *
   * @return {array} Ticket array containing n numbers where i = n - 1 for ease of access;
   */
  parseTicket(ticketString) {
    let ticket = [];
    if (ticketString.length % (this.numbersPerTicket * 2) === 0) {
      ticket = Array(ticketString.length / 2);
      for (let i = 0; i < ticketString.length; i = i + 2) {
        const number = parseInt(ticketString[i] + ticketString[i + 1], 10);
        const column = number <= 9 ? 1 : parseInt(String(number)[0], 10) + 1;
        ticket[number - 1] = {
          line: Math.floor((i / 2) / this.numbersPerLine) + 1,
          called: false,
          number,
          column: column <= 9 ? column : 9
        };
      }
    }
    return ticket;
  }

  /**
   * claim - Marks the specified number as called
   *
   * @param {int} number The number to claim
   *
   * @return {object} Number object representing the claimed number
   */
  claim(number) {
    this.ticketArray[number - 1].called = true;
    return this.ticketArray[number - 1];
  }

  /**
   * getLine - Returns an array containing the numbers in the specified line
   *
   * @param {int} index The line to return, not zero indexed (first line is 1)
   *
   * @return {array} Subset of the ticket from the specified line
   */
  getLine(index) {
    return this.ticketArray.filter(n => n.line === index);
  }

  /**
   * checkLine - Returns the number of numbers yet to be claimed on the line
   *
   * @param {int} index The line to check, not zero indexed (first line is 1)
   *
   * @return {int} Returns the amount of number left on the line
   */
  checkLine(index) {
    const line = this.getLine(index);
    let left = this.numbersPerLine;
    for (let i = 0; i < line.length; ++i) {
      if (line[i].called) left--;
    }
    return left;
  }

  /**
   * checkTicket - Checks the specified tiket for a win
   *
   * @param {int} index The ticket to check, not zero indexed (first ticket is 1)
   *
   * @return {boolean} True if the ticket is a win, false otherwise
   */
  checkTicket(index) {
    let left = 0;
    const firstLineIndex = (index - 1) * this.linesPerTicket;
    for (let i = firstLineIndex; i < firstLineIndex + this.linesPerTicket; ++i) {
      left += this.checkLine(i + 1);
    }
    return left;
  }
}

export default TicketStrip;

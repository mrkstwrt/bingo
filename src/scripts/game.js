import Caller from './components/caller';
import TicketStrip from './components/ticket';

class Game {

  /**
   * constructor - Sets up a new game
   *
   * @param {element}   element             The DOM element to initialise the game inside
   * @param {string}   ticketString        String representing the numbers on the ticket
   * @param {number} [callInterval=1000] Interval between each step of the game, default 1 second
   */
  constructor(element, ticketString, callInterval = 1000) {
    this.element = document.querySelector(element);

    if (this.element) {
      this.ticketString = ticketString;
      this.callInterval = callInterval;
      this.console = document.querySelector('.ui .console');
      this.startButton = document.querySelector('button[data-id="start"]');
      this.resetButton = document.querySelector('button[data-id="reset"]');
      this.ballElement = document.querySelector('.ui .ball');
      this.welcomeMessage = this.console.textContent;
      this.ballColours = ['red', 'pink', 'purple', 'blue', 'orange', 'green'];
      this.addEventListeners();
    } else {
      throw new Error('No Element');
    }
  }

  /**
   * init - Initialises a new game, can be called at any time to reset for a new game
   */
  init() {
    try {
      this.ticket = new TicketStrip(this.ticketString);
    } catch (e) {
      console.log(e.message);
    }

    clearInterval(this.gameInterval);
    this.updateBall('?');
    this.consoleMessage(this.welcomeMessage);
    this.buildTemplate();
    this.renderTicket();
    this.startButton.removeAttribute('disabled');
  }

  /**
   * addEventListeners - Adds event listeners to ui elements
   */
  addEventListeners() {
    this.resetButton.addEventListener('click', () => {
      this.init();
    });
    this.startButton.addEventListener('click', () => {
      this.start();
    });
  }

  /**
   * buildTemplate - Builds a ticket template inside the game element
   */
  buildTemplate() {
    const frag = document.createDocumentFragment();
    let lineCount = 1;
    let columnCount = 1;
    for (let i = 1; i <= this.ticket.numberOfTickets; ++i) {
      const ticket = document.createElement('div');
      ticket.dataset.id = i;
      ticket.classList.add('ticket');
      const numbers = document.createElement('div');
      numbers.classList.add('numbers');
      for (let n = 1; n <= 27; ++n) {
        const number = document.createElement('div');
        number.classList.add('number');
        number.dataset.id = `${lineCount}-${columnCount}`;
        numbers.appendChild(number);
        if (n % 9 === 0 && n > 1) {
          lineCount++;
          columnCount = 1;
        } else {
          columnCount++;
        }
      }
      ticket.appendChild(numbers);
      const counter = document.createElement('div');
      counter.classList.add('counter');
      ticket.appendChild(counter);
      frag.appendChild(ticket);
    }
    while (this.element.firstChild) {
      this.element.removeChild(this.element.firstChild);
    }
    this.element.appendChild(frag);
  }

  /**
   * renderTicket - Uses the ticket object to populate the template with numbers
   */
  renderTicket() {
    for (const n of this.ticket.ticketArray) {
      this.element.querySelector(`.number[data-id='${n.line}-${n.column}']`).textContent = n.number;
    }
    this.checkTickets();
  }

  /**
   * dabTicket - 'Dabs' a given number on the tickets
   *
   * @param {int} number The number to dab
   */
  dabTicket(number) {
    this.element.querySelector(`.number[data-id="${number.line}-${number.column}"]`).classList.add('called');
  }

  /**
   * checkTickets - Checks all tickets for a winner (full-house, all numbers on a single ticket dabbed)
   *
   * @return {int} Returns 0 for no win or a winning ticket number
   */
  checkTickets() {
    let winner = 0;
    for (let i = 1; i <= this.ticket.numberOfTickets; ++i) {
      const left = this.ticket.checkTicket(i);
      this.element.querySelector(`.ticket[data-id="${i}"] > .counter`).textContent = left;
      if (left === 0) winner = i;
    }
    return winner;
  }

  /**
   * updateBall - Updates the ball graphic with a given number
   *
   * @param {string|number} number The number or string to render in the ball
   */
  updateBall(number) {
    this.ballElement.textContent = number;
    this.ballElement.classList.remove(...this.ballColours);
    this.ballElement.classList.add(this.ballColours[Math.floor(Math.random() * this.ballColours.length)]);
  }

  /**
   * gameStep - Runs one step of the game, calls a new ball and checks for winners
   */
  gameStep() {
    const ball = this.caller.pick();
    if (ball > 0) {
      this.updateBall(ball);
      this.consoleMessage(`${ball} called!`);
      this.dabTicket(this.ticket.claim(ball));
      const winCheck = this.checkTickets();
      if (winCheck > 0) {
        this.finishGame(winCheck);
      }
    } else {
      this.consoleMessage('Game Over? Not Possilbe!');
      clearInterval(this.gameInterval);
    }
  }

  /**
   * consoleMessage - Helper function for displaying messages in the ui
   *
   * @param {type} message The message to display
   */
  consoleMessage(message) {
    this.console.textContent = message;
  }

  /**
   * start - Starts a new game, should be called after init()
   */
  start() {
    this.startButton.setAttribute('disabled', true);
    this.caller = new Caller();
    this.gameInterval = setInterval(() => {
      this.gameStep();
    }, this.callInterval);
  }

  /**
   * finishGame - Ends the game and stamps the winning ticket
   *
   * @param {type} winningTicket The ticket number of the winning ticket
   */
  finishGame(winningTicket) {
    clearInterval(this.gameInterval);
    this.element.querySelector(`.ticket[data-id="${winningTicket}"]`).classList.add('winner');
    this.consoleMessage(`Winner on ticket number ${winningTicket}!`);
  }
}

export default Game;

const gulp = require('gulp');
const rollup = require('rollup').rollup;
const babel = require('rollup-plugin-babel');
const uglify = require('rollup-plugin-uglify');
const runSequence = require('run-sequence');
const del = require('del');
const connect = require('gulp-connect');
const sass = require('gulp-sass');
const nano = require('gulp-cssnano');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('scripts', () => {
  rollup({
    entry: 'src/scripts/main.js',
    plugins: [
      babel({
        exclude: 'node_modules/**',
        presets: 'es2015-rollup'
      }),
      uglify()
    ]
  })
  .then(bundle => {
    bundle.write({
      format: 'iife',
      moduleName: 'Tombola Interview App',
      dest: 'dist/scripts/app.js',
      sourceMap: true
    });
  });
});

gulp.task('index', () => {
  gulp.src('src/html/index.html')
    .pipe(gulp.dest('dist'));
});

gulp.task('sass', () => {
  gulp.src('src/sass/main.scss')
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(nano())
    .pipe(gulp.dest('dist/css'));
});

gulp.task('watch', () => {
  gulp.watch('src/scripts/**/*.js', ['scripts']);
  gulp.watch('src/html/index.html', ['index']);
  gulp.watch('src/sass/**/*.scss', ['sass']);
});

gulp.task('clean', () => {
  del.sync(['dist']);
});

gulp.task('build', () => {
  runSequence('clean', ['index', 'sass', 'scripts']);
});

gulp.task('connect', () => {
  connect.server({
    root: 'dist'
  });
});

gulp.task('default', () => {
  runSequence('build', 'connect', 'watch');
});
